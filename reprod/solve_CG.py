from ngsolve import *
from xfem import *
from ngsolve.solvers import *
from ngsolve.meshes import *
from math import pi
from xfem.lset_spacetime import *

def SolveCG(i_t=2, i_s=2, k_t = 1, k_t_lset = 1, k_s = 3, geom='kite', gamma=0.05, struct_mesh = 1, eps_f=1.1, n_threads = 6, solver="umfpack", tp_gp = False, exact_nze_est = False):

    #ngsglobals.msg_level = 1
    SetNumThreads(n_threads)

    n_steps = 2**(i_t if struct_mesh == 1 else i_t+2)
    space_refs = i_s

    # Polynomial order in time for level set approximation
    lset_order_time = k_t_lset
    # Integration order in time
    time_order = 2 * max( k_t, k_t_lset) +2
    # Time stepping parameters
    tstart = 0
    tend = 0.5
    delta_t = (tend - tstart) / n_steps
    # Second Ghost-penalty parameter
    gamma_ext = gamma
    # Map from reference time to physical time
    told = Parameter(tstart)
    t = told + delta_t * tref

    if geom == 'moving_circle':
        from netgen.geom2d import SplineGeometry
        # Outer domain:
        geometry = SplineGeometry()
        geometry.AddRectangle([-0.6, -1], [0.6, 1])
        maxh = 0.5
        
        D = 2
        
        (xmin, xmax) = (-0.6, 0.6)
        (ymin, ymax) = (-1.1, 1.1)

        # Level set geometry
        # Radius of disk (the geometry)
        R = 0.5
        # Position shift of the geometry in time
        rho = (1 / (pi)) * sin(2 * pi * t)
        # Convection velocity:
        w = CoefficientFunction((0, rho.Diff(t)))
        max_velocity = 2
        # Level set
        r = sqrt(x**2 + (y - rho)**2)
        levelset = r - R

        # Diffusion coefficient
        alpha = 1
        # Solution
        u_exact = cos(pi * r / R) * sin(pi * t)
        # R.h.s.
        #coeff_f = (u_exact.Diff(t)
                #- alpha * (u_exact.Diff(x).Diff(x) + u_exact.Diff(y).Diff(y))
                #+ w[0] * u_exact.Diff(x) + w[1] * u_exact.Diff(y)).Compile()
        Q = pi/R
        coeff_f = ( IfPos(r-1e-9,Q/r*sin(Q*r),Q**2) + (Q**2) * cos(Q*r)) * sin(pi*t) + pi * cos(Q*r) * cos(pi*t)
    
    elif geom == 'moving_int':
        D = 1
        
        (xmin, xmax) = (-1, 1)
        #(xmin, xmax) = (-2, 2)

        # Level set geometry
        # Radius of disk (the geometry)
        R = 0.5
        ## Position shift of the geometry in time
        #v = 2.
        #rho = v*t - 1./16#*1e-6 #(1 / (pi)) * sin(2 * pi * t)
        #R = 0.495 #0.5 - 0.043592452943
        # Position shift of the geometry in time
        rho = (1 / (pi)) * sin(2 * pi * t)
        # Convection velocity:
        w = CoefficientFunction((rho.Diff(t)))
        max_velocity = 2. #1.5*v
        
        # Level set
        r = sqrt((x-rho)**2)
        levelset = r - R

        # Diffusion coefficient
        alpha = 1
        # Solution
        u_exact = cos(pi * r / (R) ) * sin(pi * t)
        # R.h.s.
        #dudt = cos(pi*r/R)*pi*cos(pi*t) + sin(pi*r/R)*pi/R*sin(pi*t)*2*IfPos(x - 2*t, 1, -1)
        #coeff_f = u_exact.Diff(t) - alpha *(u_exact.Diff(x).Diff(x)) + w[0] * u_exact.Diff(x)#.Compile()
        Q = pi/R
        coeff_f = ((Q**2) * cos(Q*r)) * sin(pi*t) + pi * cos(Q*r) * cos(pi*t)
    
    elif geom == 'kite':
        from netgen.geom2d import SplineGeometry
        geometry = SplineGeometry()
        geometry.AddRectangle([-1.05,-1.05],[1.55,1.05])
        maxh = 0.9
        
        D = 2
        
        (xmin, xmax) = (-1.05, 1.55)
        (ymin, ymax) = (-1.05, 1.55)
        
        r0 = 1
    
        rho = (1 - y**2)*t                                   
    
        alpha = 1
        #convection velocity:
        w = CoefficientFunction((rho.Diff(t),0))
        max_velocity = 1
        
        # level set
        r = sqrt((x- rho)**2+y**2)
        levelset= r - r0

        Q = pi/r0   
        u_exact = cos(Q*r) * sin(pi*t)
        
        coeff_f = (u_exact.Diff(t)
                - alpha * (u_exact.Diff(x).Diff(x) + u_exact.Diff(y).Diff(y))
                + w[0] * u_exact.Diff(x) + w[1] * u_exact.Diff(y)).Compile()
    
    elif geom == 'moving_sphere':
        from ngsolve.meshes import OrthoBrick, Pnt, CSGeometry
        geometry = CSGeometry()
        geometry.Add (OrthoBrick(Pnt(-0.6,-1,-0.6), Pnt(0.6,1,0.6)))
        maxh = 0.5
        
        D = 3
        
        (xmin, xmax) = (-0.6, 0.6)
        (ymin, ymax) = (-0.85, 0.85)
        (zmin, zmax) = (-0.6, 0.6)
        
        # Level set geometry
        # Radius of sphere (the geometry)
        R = 0.5
        # Position shift of the geometry in time
        rho = (1 / (pi)) * sin(2 * pi * t)
        # Convection velocity:
        w = CoefficientFunction((0, rho.Diff(t), 0))
        max_velocity = 2
        # Level set
        r = sqrt(x**2 + (y - rho)**2 + z**2)
        levelset = r - R

        # Diffusion coefficient
        alpha = 1
        # Solution
        u_exact = cos(pi * r / R) * sin(pi * t)
        # R.h.s.
        coeff_f = (u_exact.Diff(t)
                - alpha * (u_exact.Diff(x).Diff(x) + u_exact.Diff(y).Diff(y) + u_exact.Diff(z).Diff(z))
                + w[0] * u_exact.Diff(x) + w[1] * u_exact.Diff(y) + w[2] * u_exact.Diff(z)).Compile()
        
    
    # ----------------------------------- MAIN ------------------------------------
    if struct_mesh == 1:
        if D == 1:
            mesh = Make1DMesh(n=2**(space_refs+1), mapping= lambda x : (xmax - xmin) *x + xmin)
        elif D == 2:
            mesh = MakeStructured2DMesh(quads=False,nx=2**(space_refs+1),ny=2**(space_refs+1), mapping= lambda x,y : ( (xmax - xmin) *x + xmin, (ymax - ymin) *y + ymin))
        elif D == 3:
            mesh = MakeStructured3DMesh(hexes=False,nx=2**(space_refs+1),ny=2**(space_refs+1), nz=2**(space_refs+1), mapping= lambda x,y,z : ( (xmax - xmin) *x + xmin, (ymax - ymin) *y + ymin, (zmax - zmin) *z + zmin))
        else:
            Exception("Dims 2 and 3 allowed only")
    elif struct_mesh == 0:
        ngmesh = geometry.GenerateMesh(maxh=maxh*0.5**space_refs, quad_dominated=False)
        #ngmesh = geometry.GenerateMesh(maxh=maxh, quad_dominated=False)
        #for j in range(space_refs):
            #ngmesh.Refine()
        mesh = Mesh(ngmesh)
    elif struct_mesh == 2:
        mesh = Mesh("mesh_"+geom+"i_s"+str(space_refs)+".vol.gz")
    
    # Spatial FESpace for solution
    fes = H1(mesh, order=k_s, dgjumps=True)
    # Time finite elements (nodal!)
    tfe_i = ScalarTimeFE(k_t, skip_first_node=True)  # interior shapes
    tfe_e = ScalarTimeFE(k_t, only_first_node=True)  # exterior shapes (init. val.)
    tfe_t = ScalarTimeFE(k_t - 1)                    # test shapes
    # Space-time finite element space
    st_fes_i, st_fes_e, st_fes_t = [tfe * fes for tfe in [tfe_i, tfe_e, tfe_t]]

    # Space time version of Levelset Mesh Adaptation object. Also offers integrator
    # helper functions that involve the correct mesh deformation
    lsetadap = LevelSetMeshAdaptation_Spacetime(mesh, order_space=k_s,
                                                order_time=lset_order_time,
                                                threshold=0.5,
                                                discontinuous_qn=True)

    # lset epsilon perturbation for extended facet patch bfi domain
    eps = eps_f * max_velocity * delta_t
    print("eps : ", eps, "maxh: ", 0.5**space_refs, "delta_t", delta_t)

    gfu_i = GridFunction(st_fes_i)
    gfu_e = GridFunction(st_fes_e)

    u_last = CreateTimeRestrictedGF(gfu_e, 0)

    u_i = st_fes_i.TrialFunction()
    u_e = st_fes_e.TrialFunction()
    v_t = st_fes_t.TestFunction()

    if mesh.dim > 1:
        scene = DrawDC(lsetadap.levelsetp1[TOP], u_last, 0, mesh, "u_last",
                       deformation=lsetadap.deformation[TOP])
        scene2 = DrawDC(fix_tref(levelset,1), u_last - fix_tref(u_exact,1), 0, mesh, "error",
                        deformation=lsetadap.deformation[TOP])

    lset_p1_slice = GridFunction(lsetadap.levelsetp1[BOTTOM].space)

    h = specialcf.mesh_size

    ba_strip = BitArray(mesh.ne)
    ba_plus_hasneg = BitArray(mesh.ne)
    ba_minus_haspos = BitArray(mesh.ne)
    ba_facets_vol = BitArray(mesh.nfacet)
    ba_facets_end = BitArray(mesh.nfacet)
    ba_facets_vol_or_end = BitArray(mesh.nfacet)
    ci = CutInfo(mesh, time_order=time_order)
    ci_slice = CutInfo(mesh)

    dQ = delta_t * dCut(lsetadap.levelsetp1[INTERVAL], NEG, time_order=time_order,
                        deformation=lsetadap.deformation[INTERVAL],
                        definedonelements=ci.GetElementsOfType(HASNEG))
    dOmnew = dCut(lsetadap.levelsetp1[TOP], NEG,
                deformation=lsetadap.deformation[TOP],
                definedonelements=ci.GetElementsOfType(HASNEG), tref=1)
    dw_vol = delta_t * dFacetPatch(definedonelements=ba_facets_vol, time_order=time_order,
                            deformation=lsetadap.deformation[INTERVAL])
    dw_end = delta_t * dFacetPatch(definedonelements=ba_facets_end,
                            deformation=lsetadap.deformation[TOP], tref=1)

    def dt(u):
        return 1.0 / delta_t * dtref(u)


    a_i = RestrictedBilinearForm(trialspace=st_fes_i, testspace=st_fes_t,
                                element_restriction=ci.GetElementsOfType(HASNEG),
                                facet_restriction=( ba_facets_vol if tp_gp else ba_facets_vol_or_end),
                                check_unused=False)

    a_i += v_t * (dt(u_i) - dt(lsetadap.deform) * grad(u_i)) * dQ
    a_i += (alpha * InnerProduct(grad(u_i), grad(v_t))) * dQ
    a_i += (v_t * InnerProduct(w, grad(u_i))) * dQ
    a_i += h**(-2) * (1 + delta_t / h) * gamma * \
        (u_i - u_i.Other()) * (v_t - v_t.Other()) * dw_vol
    if not tp_gp:
        a_i +=  h**(-2) * (1 + delta_t / h) * gamma_ext * \
            (u_i - u_i.Other()) * (v_t - v_t.Other())  * dw_end

    f = LinearForm(st_fes_t)
    f += coeff_f * v_t * dQ
    f += -v_t * (dt(gfu_e) - dt(lsetadap.deform) * grad(gfu_e)) * dQ
    f += -(alpha * InnerProduct(grad(gfu_e), grad(v_t))) * dQ
    f += -(v_t * InnerProduct(w, grad(gfu_e))) * dQ

    ba_plus_hasneg_old, els_test = BitArray(mesh.ne), BitArray(mesh.ne)
    ba_plus_hasneg_old.Set()
    
    # Set initial values
    u_last.Set(fix_tref(u_exact, 0))
    # Project u_last at the beginning of each time step
    lsetadap.ProjectOnUpdate(u_last, ba_plus_hasneg_old)
    
    l2l2_helper_sum = 0
    firstrun = True
    
    while tend - told.Get() > delta_t / 2:
        lsetadap.CalcDeformation(levelset)
        
        gfu_e.vec[:].data = u_last.vec
        
        navie_timeint_tmp = ngsxfemglobals.do_naive_timeint
        ngsxfemglobals.do_naive_timeint = False
        # Update markers in (space-time) mesh
        ci.Update(lsetadap.levelsetp1[INTERVAL], time_order=0)
        ngsxfemglobals.do_naive_timeint = navie_timeint_tmp

        # Re-evaluate the "active dofs" in the space time slab
        InterpolateToP1(lsetadap.levelsetp1[TOP] - eps, lset_p1_slice)
        ci_slice.Update(lset_p1_slice)
        ba_plus_hasneg[:] = ci_slice.GetElementsOfType(HASNEG)

        InterpolateToP1(lsetadap.levelsetp1[TOP] + eps, lset_p1_slice)
        ci_slice.Update(lset_p1_slice)
        ba_minus_haspos[:] = ci_slice.GetElementsOfType(HASPOS)

        ba_strip[:] = ba_minus_haspos & ba_plus_hasneg
        if tp_gp:
            ba_facets_vol[:] = GetFacetsWithNeighborTypes(mesh, a=ba_strip, b=ba_plus_hasneg)
            active_dofs = GetDofsOfElements(st_fes_i, ba_plus_hasneg)
        else:
            ba_facets_end[:] = GetFacetsWithNeighborTypes(mesh, a=ba_strip,
                                                    b=ba_plus_hasneg)
            ba_facets_vol[:] = GetFacetsWithNeighborTypes(mesh,
                                                a=ci.GetElementsOfType(HASNEG),
                                                b=ci.GetElementsOfType(IF))
            ba_facets_vol_or_end[:] = ba_facets_vol | ba_facets_end
            
            active_dofs = GetDofsOfElements(st_fes_i, ci.GetElementsOfType(HASNEG))
            active_dofs[-fes.ndof:] = GetDofsOfElements(fes, GetElementsWithNeighborFacets(mesh, ba_facets_end)) | GetDofsOfElements(fes, ci.GetElementsOfType(HASNEG))
        
        # Check element history for method of lines time-derivative approx.
        els_test[:] = ci.GetElementsOfType(HASNEG) & ~ba_plus_hasneg_old
        assert sum(els_test) == 0, 'Some active elements do not have a history.\
            You might want to increase eps'

        ba_plus_hasneg_old[:] = ba_plus_hasneg
        
        with TaskManager():
            a_i.Assemble(reallocate=True)
            f.Assemble()

        # Solve linear system
        inv = a_i.mat.Inverse(active_dofs, solver)
        gfu_i.vec.data = GMRes(a_i.mat, f.vec, inv, tol=1e-12, printrates=False,maxsteps=50)
        #gfu_i.vec.data = a_i.mat.Inverse(active_dofs) * f.vec

        # Evaluate upper trace of solution for
        #  * for error evaluation
        #  * upwind-coupling to next time slab
        RestrictGFInTime(spacetime_gf=gfu_i, reference_time=1.0, space_gf=u_last)
        
        # Compute error at final time
        l2error = sqrt(Integrate(IfPos(levelset,0, (u_exact - u_last)**2) * dCut(lsetadap.levelsetp1[TOP], NEG, deformation=lsetadap.deformation[TOP], definedonelements=ci.GetElementsOfType(HASNEG), tref=1, order=2*k_s), mesh))
        
        l2l2_helper_sum += Integrate((u_exact - gfu_i - gfu_e)**2 * delta_t * dCut(lsetadap.levelsetp1[INTERVAL], NEG, time_order=time_order, deformation=lsetadap.deformation[INTERVAL], definedonelements=ci.GetElementsOfType(HASNEG), order=2*k_s), mesh)
        
        if exact_nze_est:
            cnt=0
            for ii in range(len(a_i.mat.AsVector())):
                if (a_i.mat.AsVector()[ii] != 0.0):
                    cnt += 1
        else:
            cnt = a_i.mat.nze
        
        if firstrun:
            minnze = cnt
            maxnze = cnt
            firstrun = False
        else:
            if cnt > maxnze:
                maxnze = cnt
            if cnt < minnze:
                minnze = cnt
                
        Redraw()
        
        # Update time variable (ParameterCF)
        told.Set(told.Get() + delta_t)
        #break
        print("\rt = {0:12.9f}, L2 error = {1:12.9e}".format(told.Get(), l2error))
        
    print("Final l2l2 error", sqrt(l2l2_helper_sum))
    return (l2error, sqrt(l2l2_helper_sum), minnze, maxnze)
