from solve_DG import *
from solve_CG import *

import argparse

parser = argparse.ArgumentParser(description='Solve convection-diffusion problem on a moving domain by a spacetime method. NZE investigation')
parser.add_argument('-g','--geometry', default="kite", help ='Used geometry, options moving_circle, kite, moving_sphere')
parser.add_argument('-ks','--ks', type=int, default=3, help='order of space discretisation & isoparam mapping, default: 3')
parser.add_argument('-kt','--kt', type=int, default=3, help='order of time discretisation, default: 3. For GCC, 3 will be used independent of this parameter.')
parser.add_argument('-ktls','--ktls', type=int, default=3, help='order of time discretisation for lset deformation, default: 3.')
parser.add_argument('-is','--is', type=int, default=3, help='spatial refinement level, default: 4')
parser.add_argument('-it','--it', type=int, default=3, help='temporal refinement level, default: 4')
parser.add_argument('-gamma','--gamma', type=float, default=0.05, help='Ghost Penalty stabilisation constant, default: 0.05')
parser.add_argument('-sm','--struct_mesh', type=int, default=1, help='Use a structured mesh, enter either int 0 for false or int 1 for true. default: 1 - true')
parser.add_argument('-tp_gp','--tp_gp', type=int, default=0, help='For the CG method, a tensor product Ghost penalty will be used for tp_gp = 1, and a reduced domain GP for tp_gp = 0. default: 0')

args = parser.parse_args()
options = vars(args)

outfile_name_CG = "out/nze_"+str(options["geometry"])+"_CG_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_ktls"+str(options["ktls"])+"tp_gp"+str(options["tp_gp"])+".dat"
f = open(outfile_name_CG, "w")
f.close()

outfile_name_DG = "out/nze_"+str(options["geometry"])+"_DG_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_ktls"+str(options["ktls"])+".dat"
f = open(outfile_name_DG, "w")
f.close()
l2finalDG, l2l2erDG, minnzesDG, maxnzesDG = SolveDG(i_t=options["it"], i_s=options["is"], k_t = options["kt"], k_t_lset = options["ktls"], k_s = options["ks"], geom=options["geometry"], gamma=options["gamma"], struct_mesh=options["struct_mesh"], exact_nze_est=True)
print("Single run result (DG): ", l2finalDG, l2l2erDG, minnzesDG, maxnzesDG)

eps_vals = [1, 1.1, 1.5, 2, 4, 50, 100]

for eps in eps_vals:
    l2final, l2l2er, minnzes, maxnzes = SolveCG(i_t=options["it"], i_s=options["is"], k_t = options["kt"], k_t_lset = options["ktls"], k_s = options["ks"], geom=options["geometry"], gamma=options["gamma"], struct_mesh=options["struct_mesh"], eps_f=eps, tp_gp = (bool)(options["tp_gp"]), exact_nze_est=True)
    print("Single run result (CG): ", l2final, l2l2er, minnzes, maxnzes)
    f = open(outfile_name_CG, "a")
    f.write(str(eps)+"\t"+str(l2final)+"\t"+str(l2l2er)+"\t"+str(minnzes)+"\t"+str(maxnzes)+"\n")
    f.close()
    f = open(outfile_name_DG, "a")
    f.write(str(eps)+"\t"+str(l2finalDG)+"\t"+str(l2l2erDG)+"\t"+str(minnzesDG)+"\t"+str(maxnzesDG)+"\n")
    f.close()
