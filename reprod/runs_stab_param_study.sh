#!/bin/bash
#SBATCH -p medium
#SBATCH -t 10:00:00
#SBATCH -N 1
#SBATCH -n 8
#SBATCH -C cascadelake

source ~/.bashrc
 
/usr/bin/time -v python3 conv_study.py --method DG --ks 4 --kt 4 --ktls 4 --n_ref 5 --struct_mesh 2 --gamma 5e4
/usr/bin/time -v python3 conv_study.py --method DG --ks 4 --kt 4 --ktls 4 --n_ref 5 --struct_mesh 2 --gamma 5e0
/usr/bin/time -v python3 conv_study.py --method DG --ks 4 --kt 4 --ktls 4 --n_ref 5 --struct_mesh 2 --gamma 5e-2
/usr/bin/time -v python3 conv_study.py --method DG --ks 4 --kt 4 --ktls 4 --n_ref 5 --struct_mesh 2 --gamma 5e-4
/usr/bin/time -v python3 conv_study.py --method DG --ks 4 --kt 4 --ktls 4 --n_ref 5 --struct_mesh 2 --gamma 5e-10
/usr/bin/time -v python3 conv_study.py --method DG --ks 4 --kt 4 --ktls 4 --n_ref 3 --struct_mesh 2 --gamma 0
