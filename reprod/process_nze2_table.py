import numpy as np

def int_to_str_special(i):
    if i < 1000:
        return str(i)
    elif i < 1e4:
        return str((int)(i/1e3))+"."+str((int)( (i - 1e3*((int)(i/1e3)))/10 ) )+"K"
    elif i < 1e5:
        return str((int)(i/1e3))+"."+str((int)( (i - 1e3*((int)(i/1e3)))/100 ) )+"K"
    elif i < 1e6:
        return str((int)(i/1e3))+"K"
    elif i < 1e7:
        return str((int)(i/1e6))+"."+str((int)( (i - 1e6*((int)(i/1e6)))/1e4 ) )+"M"
    elif i < 1e8:
        return str((int)(i/1e6))+"."+str((int)( (i - 1e6*((int)(i/1e6)))/1e5 ) )+"M"
    elif i < 1e9:
        return str((int)(i/1e6))+"M"

def float_to_str_special(f):
    float_str = "{0:.1e}".format(f)
    if "e" in float_str:
        base, exponent = float_str.split("e")
        return r"${0}\!\cdot\!10^{{{1}}}$".format(base, int(exponent))
    else:
        return float_str

def gen_table(order):
    if order == 3:
        incl_GCC = True
    else:
        incl_GCC = False
        
    nzes_DG_min = np.loadtxt("out/nze2_moving_circle_DG_ks"+str(order)+"_kt"+str(order)+"_ktls"+str(order)+".min.dat", dtype=int)
    nzes_DG_max = np.loadtxt("out/nze2_moving_circle_DG_ks"+str(order)+"_kt"+str(order)+"_ktls"+str(order)+".max.dat", dtype=int)
    l2f_DG = np.loadtxt("out/nze2_moving_circle_DG_ks"+str(order)+"_kt"+str(order)+"_ktls"+str(order)+".l2f.dat")
    l2l2_DG = np.loadtxt("out/nze2_moving_circle_DG_ks"+str(order)+"_kt"+str(order)+"_ktls"+str(order)+".l2l2.dat")

    nzes_CG_min = np.loadtxt("out/nze2_moving_circle_CG_ks"+str(order)+"_kt"+str(order)+"_ktls"+str(order)+".min.dat", dtype=int)
    nzes_CG_max = np.loadtxt("out/nze2_moving_circle_CG_ks"+str(order)+"_kt"+str(order)+"_ktls"+str(order)+".max.dat", dtype=int)
    l2f_CG = np.loadtxt("out/nze2_moving_circle_CG_ks"+str(order)+"_kt"+str(order)+"_ktls"+str(order)+".l2f.dat")
    l2l2_CG = np.loadtxt("out/nze2_moving_circle_CG_ks"+str(order)+"_kt"+str(order)+"_ktls"+str(order)+".l2l2.dat")

    if incl_GCC:
        nzes_GCC_min = np.loadtxt("out/nze2_moving_circle_GCC_ks"+str(order)+"_kt"+str(order)+"_ktls"+str(order)+".min.dat", dtype=int)
        nzes_GCC_max = np.loadtxt("out/nze2_moving_circle_GCC_ks"+str(order)+"_kt"+str(order)+"_ktls"+str(order)+".max.dat", dtype=int)
        
        l2f_GCC = np.loadtxt("out/nze2_moving_circle_GCC_ks"+str(order)+"_kt"+str(order)+"_ktls"+str(order)+".l2f.dat")
        l2l2_GCC = np.loadtxt("out/nze2_moving_circle_GCC_ks"+str(order)+"_kt"+str(order)+"_ktls"+str(order)+".l2l2.dat")
    
    outfile = open("nze2_table_k"+str(order)+".tex","w")
    outfile.write("DG($k\\!=\\!"+str(order)+"$): \n")
    for j in range(nzes_DG_min.shape[0]):
        outfile.write("& "+int_to_str_special(nzes_DG_min[j])+"&"+int_to_str_special(nzes_DG_max[j])+" & ")
        outfile.write(" "+float_to_str_special(l2f_DG[j])+"\n")
    outfile.write("\\\\ \n")

    outfile.write("CG($k\\!=\\!"+str(order)+"$): \n")
    for j in range(nzes_DG_min.shape[0]):
        outfile.write("& "+int_to_str_special(nzes_CG_min[j])+"&"+int_to_str_special(nzes_CG_max[j])+" & ")
        outfile.write(" "+float_to_str_special(l2f_CG[j])+"\n")
    outfile.write("\\\\ \n")

    if incl_GCC:
        outfile.write("GCC($k\\!=\\!"+str(order)+"$): \n")
        for j in range(nzes_DG_min.shape[0]):
            outfile.write("& "+int_to_str_special(nzes_GCC_min[j])+"&"+int_to_str_special(nzes_GCC_max[j])+" & ")
            outfile.write(" "+float_to_str_special(l2f_GCC[j])+"\n")
        outfile.write("\\\\ \n")

gen_table(1)
#gen_table(2)
gen_table(3)
#gen_table(4)
gen_table(5)
