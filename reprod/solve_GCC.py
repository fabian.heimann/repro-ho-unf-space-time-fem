from ngsolve import *
from xfem import *
from ngsolve.solvers import *
from ngsolve.meshes import *
from math import pi
from xfem.lset_spacetime import *

def SolveGCC(i_t=2, i_s=2, k_t = 3, k_t_lset = 1, k_s = 3, geom='kite', gamma=0.05, struct_mesh = 1, eps_f = 1.1, n_threads = 6, solver="umfpack", exact_nze_est = False):
    
    #ngsglobals.msg_level = 2
    SetNumThreads(n_threads)

    n_steps = 2**(i_t if struct_mesh == 1 else i_t+2)
    space_refs = i_s

    # Polynomial order in time for level set approximation
    lset_order_time = k_t_lset
    # Integration order in time
    time_order = 2 * max( k_t, k_t_lset) +2
    # Time stepping parameters
    tstart = 0
    tend = 0.5
    delta_t = (tend - tstart) / n_steps
    # Map from reference time to physical time
    told = Parameter(tstart)
    t = told + delta_t * tref

    if geom == 'moving_circle':
        from netgen.geom2d import SplineGeometry
        # Outer domain:
        geometry = SplineGeometry()
        geometry.AddRectangle([-0.6, -1], [0.6, 1])
        maxh = 0.5
        
        D = 2
        
        (xmin, xmax) = (-0.6, 0.6)
        (ymin, ymax) = (-1, 1)

        # Level set geometry
        # Radius of disk (the geometry)
        R = 0.5
        # Position shift of the geometry in time
        rho = (1 / (pi)) * sin(2 * pi * t)
        # Convection velocity:
        w = CoefficientFunction((0, rho.Diff(t)))
        max_velocity = 2
        # Level set
        r = sqrt(x**2 + (y - rho)**2)
        levelset = r - R

        # Diffusion coefficient
        alpha = 1
        # Solution
        u_exact = cos(pi * r / R) * sin(pi * t)
        # R.h.s.
        coeff_f = (u_exact.Diff(t)
                - alpha * (u_exact.Diff(x).Diff(x) + u_exact.Diff(y).Diff(y))
                + w[0] * u_exact.Diff(x) + w[1] * u_exact.Diff(y)).Compile()
    
    elif geom == 'moving_int':
        D = 1
        
        (xmin, xmax) = (-1, 1)

        # Level set geometry
        # Radius of disk (the geometry)
        R = 0.5
        # Position shift of the geometry in time
        rho = (1 / (pi)) * sin(2 * pi * t)
        # Convection velocity:
        w = CoefficientFunction((rho.Diff(t)))
        max_velocity = 2
        
        # Level set
        r = sqrt((x-rho)**2)
        levelset = r - R

        # Diffusion coefficient
        alpha = 1
        # Solution
        u_exact = cos(pi * r / R) * sin(pi * t)
        # R.h.s.
        coeff_f = (u_exact.Diff(t) - alpha * (u_exact.Diff(x).Diff(x)) + w[0] * u_exact.Diff(x)).Compile()
    
    elif geom == 'kite':
        from netgen.geom2d import SplineGeometry
        geometry = SplineGeometry()
        geometry.AddRectangle([-1.05,-1.05],[1.55,1.05])
        maxh = 0.9
        
        D = 2
        
        (xmin, xmax) = (-1.05, 1.55)
        (ymin, ymax) = (-1.05, 1.55)
        
        r0 = 1
    
        rho = (1 - y**2)*t                                   
    
        alpha = 1
        #convection velocity:
        w = CoefficientFunction((rho.Diff(t),0))
        max_velocity = 1
        
        # level set
        r = sqrt((x- rho)**2+y**2)
        levelset= r - r0

        Q = pi/r0   
        u_exact = cos(Q*r) * sin(pi*t)
        
        coeff_f = (u_exact.Diff(t)
                - alpha * (u_exact.Diff(x).Diff(x) + u_exact.Diff(y).Diff(y))
                + w[0] * u_exact.Diff(x) + w[1] * u_exact.Diff(y)).Compile()
    
    elif geom == 'moving_sphere':
        from ngsolve.meshes import OrthoBrick, Pnt, CSGeometry
        geometry = CSGeometry()
        geometry.Add (OrthoBrick(Pnt(-0.6,-1,-0.6), Pnt(0.6,1,0.6)))
        maxh = 0.5
        
        D = 3
        
        (xmin, xmax) = (-0.6, 0.6)
        (ymin, ymax) = (-0.85, 0.85)
        (zmin, zmax) = (-0.6, 0.6)
        
        # Level set geometry
        # Radius of sphere (the geometry)
        R = 0.5
        # Position shift of the geometry in time
        rho = (1 / (pi)) * sin(2 * pi * t)
        # Convection velocity:
        w = CoefficientFunction((0, rho.Diff(t), 0))
        max_velocity = 2
        # Level set
        r = sqrt(x**2 + (y - rho)**2 + z**2)
        levelset = r - R

        # Diffusion coefficient
        alpha = 1
        # Solution
        u_exact = cos(pi * r / R) * sin(pi * t)
        # R.h.s.
        coeff_f = (u_exact.Diff(t)
                - alpha * (u_exact.Diff(x).Diff(x) + u_exact.Diff(y).Diff(y) + u_exact.Diff(z).Diff(z))
                + w[0] * u_exact.Diff(x) + w[1] * u_exact.Diff(y) + w[2] * u_exact.Diff(z)).Compile()
        
    
    # ----------------------------------- MAIN ------------------------------------
    if struct_mesh == 1:
        if D == 1:
            mesh = Make1DMesh(n=2**(space_refs+1), mapping= lambda x : (xmax - xmin) *x + xmin)
        elif D == 2:
            mesh = MakeStructured2DMesh(quads=False,nx=2**(space_refs+1),ny=2**(space_refs+1), mapping= lambda x,y : ( (xmax - xmin) *x + xmin, (ymax - ymin) *y + ymin))
        elif D == 3:
            mesh = MakeStructured3DMesh(hexes=False,nx=2**(space_refs+1),ny=2**(space_refs+1), nz=2**(space_refs+1), mapping= lambda x,y,z : ( (xmax - xmin) *x + xmin, (ymax - ymin) *y + ymin, (zmax - zmin) *z + zmin))
        else:
            Exception("Dims 2 and 3 allowed only")
    elif struct_mesh == 0:
        ngmesh = geometry.GenerateMesh(maxh=maxh*0.5**space_refs, quad_dominated=False)
        #ngmesh = geometry.GenerateMesh(maxh=maxh, quad_dominated=False)
        #for j in range(space_refs):
            #ngmesh.Refine()
        mesh = Mesh(ngmesh)
    elif struct_mesh == 2:
        mesh = Mesh("mesh_"+geom+"i_s"+str(space_refs)+".vol.gz")
    
    # spatial FESpace for solution
    fes = H1(mesh, order=k_s, dgjumps=True)
    # time finite elements (nodal!)
    tfe_i = GCC3FE(skip_first_nodes=True)  # interior shapes
    tfe_e = GCC3FE(only_first_nodes=True)  # exterior shapes (init. val.)
    tfe_t = ScalarTimeFE(0)                    # test shapes
    # space-time finite element space

    st_fes_i = tfe_i * fes
    st_fes_e = tfe_e * fes
    st_fes_t = tfe_t * fes

    fes_t = st_fes_t * fes

    u_i = st_fes_i.TrialFunction()
    v_t, w_t  = fes_t.TestFunction()

    # Space time version of Levelset Mesh Adaptation object. Also offers integrator
    # helper functions that involve the correct mesh deformation
    lsetadap = LevelSetMeshAdaptation_Spacetime(mesh, order_space=k_s,
                                                order_time=lset_order_time,
                                                threshold=0.5,
                                                discontinuous_qn=True)

    dfm_last_top = CreateTimeRestrictedGF(lsetadap.deform,1.0)
    
    # lset epsilon perturbation for extended facet patch bfi domain
    eps = eps_f * max_velocity * delta_t

    gfu_i = GridFunction(st_fes_i)
    gfu_e = GridFunction(st_fes_e)

    u_last = CreateTimeRestrictedGF(gfu_e)
    u_helper = GridFunction(fes)
    u_helper2 = GridFunction(fes)
    u_helper3 = GridFunction(fes)
    dtu_last = GridFunction(fes)

    if mesh.dim > 1:
        scene = DrawDC(lsetadap.levelsetp1[TOP], fix_tref(gfu_i,1), 0, mesh, "u_last",
                       deformation=lsetadap.deformation[TOP])

    lset_p1_slice = GridFunction(lsetadap.levelsetp1[TOP].space)

    h = specialcf.mesh_size

    ba_strip = BitArray(mesh.ne)
    ba_facets = BitArray(mesh.nfacet)
    ba_plus_hasneg = BitArray(mesh.ne)
    ba_minus_haspos = BitArray(mesh.ne)
    ci = CutInfo(mesh, time_order=time_order)
    ci_slice = CutInfo(mesh)

    dQ = delta_t * dCut(lsetadap.levelsetp1[INTERVAL], NEG, time_order=time_order,
                        deformation=lsetadap.deformation[INTERVAL],
                        definedonelements=ci.GetElementsOfType(HASNEG))
    dOmnew = dCut(lsetadap.levelsetp1[TOP], NEG,
                deformation=lsetadap.deformation[TOP],
                definedonelements=ci.GetElementsOfType(HASNEG), tref = 1)
    dw = delta_t * dFacetPatch(definedonelements=ba_facets, time_order=time_order,
                            deformation=lsetadap.deformation[INTERVAL])
    dwnew = delta_t* dFacetPatch(definedonelements=ba_facets,
                            deformation=lsetadap.deformation[TOP], tref=1)

    def dt(u): return 1.0 / delta_t * dtref(u)

    a_i = RestrictedBilinearForm(trialspace=st_fes_i, testspace=fes_t,
                                element_restriction=ci.GetElementsOfType(HASNEG),
                                facet_restriction=ba_facets,
                                check_unused=False)

    a_i += v_t * (dt(u_i) - dt(lsetadap.deform) * grad(u_i)) * dQ
    a_i += (alpha * InnerProduct(grad(u_i), grad(v_t))) * dQ
    a_i += (v_t * InnerProduct(w, grad(u_i))) * dQ

    a_i += w_t * (dt(u_i) - dt(lsetadap.deform) * grad(u_i)) * dOmnew
    a_i += alpha * InnerProduct(grad(u_i), grad(w_t)) * dOmnew
    a_i += w_t * InnerProduct(w, grad(u_i)) * dOmnew

    a_i += h**(-2) * (1 + delta_t / h) * gamma * \
        (u_i - u_i.Other()) * (v_t - v_t.Other()) * dw

    a_i += h**(-2) * (1 + delta_t / h) * gamma * \
        (u_i - u_i.Other()) * (w_t - w_t.Other()) * dwnew

    f = LinearForm(fes_t)
    f += coeff_f * v_t* dQ
    f += -v_t * (dt(gfu_e)- dt(lsetadap.deform) * grad(gfu_e)) * dQ
    f += -(v_t * InnerProduct(w, grad(gfu_e))) * dQ
    f += -(alpha * InnerProduct(grad(gfu_e), grad(v_t))) * dQ

    f += coeff_f * w_t * dOmnew
    f += -w_t * (dt(gfu_e)- dt(lsetadap.deform) * grad(gfu_e)) * dOmnew
    f += -alpha * InnerProduct(grad(gfu_e), grad(w_t)) * dOmnew
    f += -w_t * InnerProduct(w, grad(gfu_e)) * dOmnew

    
    lsetadap.CalcDeformation(levelset)
    mesh.SetDeformation(lsetadap.deformation[BOTTOM])
    
    # set initial values
    u_helper.Set(fix_tref(u_exact, 0))
    gfu_e.vec[0:fes.ndof].data = u_helper.vec
    if D == 1:
        u_helper.Set(delta_t*fix_tref(u_exact.Diff(t), 0) - delta_t*fix_tref(dt(lsetadap.deform) * CoefficientFunction((u_exact.Diff(x))),0))
    elif D == 2:
        u_helper.Set(delta_t*fix_tref(u_exact.Diff(t), 0) - delta_t*fix_tref(dt(lsetadap.deform) * CoefficientFunction((u_exact.Diff(x), u_exact.Diff(y))),0))
    elif D == 3:
        u_helper.Set(delta_t*fix_tref(u_exact.Diff(t), 0) - delta_t*fix_tref(dt(lsetadap.deform) * CoefficientFunction((u_exact.Diff(x), u_exact.Diff(y), u_exact.Diff(z))),0))
    #u_helper.Set(delta_t*fix_tref(u_exact.Diff(t), 0) )
    gfu_e.vec[fes.ndof:2*fes.ndof].data = u_helper.vec
    mesh.UnsetDeformation()
    # project u_last at the beginning of each time step
    #lsetadap.ProjectOnUpdate(u_last)
    
    ba_plus_hasneg_old, els_test = BitArray(mesh.ne), BitArray(mesh.ne)
    ba_plus_hasneg_old.Set()

    l2l2_helper_sum = 0
    firstrun = True

    while tend - told.Get() > delta_t / 2:
        lsetadap.CalcDeformation(levelset)
        
        if not firstrun:
            u_helper.Set(shifted_eval(u_last, back = dfm_last_top, forth = lsetadap.deformation[BOTTOM]), definedonelements=ba_plus_hasneg_old)
            gfu_e.vec[0:fes.ndof].data = u_helper.vec
            
            u_helper3.vec[:].data = u_helper.vec
            u_helper.Set(shifted_eval(dtu_last, back = dfm_last_top, forth = lsetadap.deformation[BOTTOM]), definedonelements=ba_plus_hasneg_old)
            mesh.SetDeformation(lsetadap.deformation[BOTTOM])
            u_helper2.Set(fix_tref(dt(lsetadap.deform),0) * grad(u_helper3), definedonelements=ba_plus_hasneg_old)
            mesh.UnsetDeformation()
            gfu_e.vec[fes.ndof:2*fes.ndof].data = delta_t*u_helper.vec + delta_t*u_helper2.vec
        
        navie_timeint_tmp = ngsxfemglobals.do_naive_timeint
        ngsxfemglobals.do_naive_timeint = False
        # Update markers in (space-time) mesh
        ci.Update(lsetadap.levelsetp1[INTERVAL], time_order=0)
        ngsxfemglobals.do_naive_timeint = navie_timeint_tmp
        
        # re-evaluate the "active dofs" in the space time slab
        InterpolateToP1(lsetadap.levelsetp1[TOP] - eps, lset_p1_slice)
        ci_slice.Update(lset_p1_slice)
        ba_plus_hasneg[:] = ci_slice.GetElementsOfType(HASNEG)

        InterpolateToP1(lsetadap.levelsetp1[TOP] + eps, lset_p1_slice)
        ci_slice.Update(lset_p1_slice)
        ba_minus_haspos[:] = ci_slice.GetElementsOfType(HASPOS)

        ba_strip[:] = ba_minus_haspos & ba_plus_hasneg
        ba_facets[:] = GetFacetsWithNeighborTypes(mesh, a=ba_strip,
                                                b=ba_plus_hasneg)
        active_dofs = GetDofsOfElements(st_fes_i, ba_plus_hasneg)

        ## Check element history for method of lines time-derivative approx.
        els_test[:] = ci.GetElementsOfType(HASNEG) & ~ba_plus_hasneg_old
        assert sum(els_test) == 0, 'Some active elements do not have a history. You might want to increase eps'

        ba_plus_hasneg_old[:] = ba_plus_hasneg
        
        with TaskManager():
            a_i.Assemble(reallocate=True)
            f.Assemble()

        # solve linear system
        #gfu_i.vec.data = a_i.mat.Inverse(active_dofs, "sparsecholesky") * f.vec
        inv = a_i.mat.Inverse(active_dofs, solver)
        gfu_i.vec.data = GMRes(a_i.mat, f.vec, inv, tol=1e-12, printrates=False,maxsteps=50)

        # evaluate upper trace of solution for
        #  * for error evaluation
        #  * upwind-coupling to next time slab
        #RestrictGFInTime(spacetime_gf=gfu_i, reference_time=1.0, space_gf=u_last)
        u_last.vec[:].data = gfu_i.vec[0:fes.ndof]
        
        mesh.SetDeformation(lsetadap.deformation[TOP])
        u_helper.vec[:].data = 1./delta_t*gfu_i.vec[fes.ndof:2*fes.ndof]
        dtu_last.Set(u_helper - fix_tref(dt(lsetadap.deform),1)*grad(u_last), definedonelements=ba_plus_hasneg)
        mesh.UnsetDeformation()
        
        # compute error at final time
        l2error = sqrt(Integrate((u_exact - u_last)**2 * dCut(lsetadap.levelsetp1[TOP], NEG, deformation=lsetadap.deformation[TOP], definedonelements=ci.GetElementsOfType(HASNEG), tref = 1, order=2*k_s), mesh))
        l2l2_helper_sum += Integrate((u_exact - gfu_i - gfu_e)**2 * delta_t * dCut(lsetadap.levelsetp1[INTERVAL], NEG, time_order=time_order, deformation=lsetadap.deformation[INTERVAL], definedonelements=ci.GetElementsOfType(HASNEG), order=2*k_s), mesh)
        
        if exact_nze_est:
            cnt=0
            for ii in range(len(a_i.mat.AsVector())):
                if (a_i.mat.AsVector()[ii] != 0.0):
                    cnt += 1
        else:
            cnt = a_i.mat.nze
        
        if firstrun:
            minnze = cnt
            maxnze = cnt
            firstrun = False
        else:
            if cnt > maxnze:
                maxnze = cnt
            if cnt < minnze:
                minnze = cnt
        
        # Update time variable (ParameterCF)
        told.Set(told.Get() + delta_t)
        print("\rt = {0:12.9f}, L2 error = {1:12.9e}".format(told.Get(), l2error))
        
        #break
        dfm_last_top.vec.data = lsetadap.deformation[TOP].vec
        #print("len(lsetadap.deformation[TOP].vec): ", len(lsetadap.deformation[TOP].vec))
        #print("lsetadap.deformation[TOP].vec: ",lsetadap.deformation[TOP].vec)
    return (l2error, sqrt(l2l2_helper_sum), minnze, maxnze)
