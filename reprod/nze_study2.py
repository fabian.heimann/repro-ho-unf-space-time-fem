from solve_DG import *
from solve_CG import *
from solve_GCC import *

#def Solve(method, i_t, i_s, k_t, k_t_ls, k_s, geom, eps_f= 1.1):
    #if method == "DG":
        #return SolveDG(i_t, i_s, k_t, k_t_ls, k_s, geom)
    #elif method == "CG":
        #return SolveCG(i_t, i_s, k_t, k_t_ls, k_s, geom, eps_f)
    #elif method == "GCC":
        #return SolveGCC(i_t, i_s, k_t, k_t_ls, k_s, geom, eps_f)
    #else:
        #print("Non-valid method flag ", method)

import argparse

parser = argparse.ArgumentParser(description='Solve convection-diffusion problem on a moving domain by a spacetime method. NZE investigation')
parser.add_argument('-g','--geometry', default="kite", help ='Used geometry, options moving_circle, kite, moving_sphere')
parser.add_argument('-ks','--ks', type=int, default=3, help='order of space discretisation & isoparam mapping, default: 3')
parser.add_argument('-kt','--kt', type=int, default=3, help='order of time discretisation, default: 3. For GCC, 3 will be used independent of this parameter.')
parser.add_argument('-ktls','--ktls', type=int, default=3, help='order of time discretisation for lset deformation, default: 3.')
parser.add_argument('-gcc','--gcc', type=bool, default=False, help='include GCC in the study, default: False.')
parser.add_argument('-sm','--struct_mesh', type=int, default=1, help='Use a structured mesh, enter either int 0 for false or int 1 for true. default: 1 - true')
parser.add_argument('-tp_gp','--tp_gp', type=int, default=0, help='For the CG method, a tensor product Ghost penalty will be used for tp_gp = 1, and a reduced domain GP for tp_gp = 0. default: 0')

args = parser.parse_args()
options = vars(args)

outfile_name_DG_min = "out/nze2_"+str(options["geometry"])+"_DG_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_ktls"+str(options["ktls"])+".min.dat"
f_DG_min = open(outfile_name_DG_min, "w")

outfile_name_DG_max = "out/nze2_"+str(options["geometry"])+"_DG_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_ktls"+str(options["ktls"])+".max.dat"
f_DG_max = open(outfile_name_DG_max, "w")

outfile_name_DG_l2f = "out/nze2_"+str(options["geometry"])+"_DG_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_ktls"+str(options["ktls"])+".l2f.dat"
f_DG_l2f = open(outfile_name_DG_l2f, "w")

outfile_name_DG_l2l2 = "out/nze2_"+str(options["geometry"])+"_DG_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_ktls"+str(options["ktls"])+".l2l2.dat"
f_DG_l2l2 = open(outfile_name_DG_l2l2, "w")

outfile_name_CG_min = "out/nze2_"+str(options["geometry"])+"_CG_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_ktls"+str(options["ktls"])+".min.dat"
f_CG_min = open(outfile_name_CG_min, "w")

outfile_name_CG_max = "out/nze2_"+str(options["geometry"])+"_CG_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_ktls"+str(options["ktls"])+".max.dat"
f_CG_max = open(outfile_name_CG_max, "w")

outfile_name_CG_l2f = "out/nze2_"+str(options["geometry"])+"_CG_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_ktls"+str(options["ktls"])+".l2f.dat"
f_CG_l2f = open(outfile_name_CG_l2f, "w")

outfile_name_CG_l2l2 = "out/nze2_"+str(options["geometry"])+"_CG_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_ktls"+str(options["ktls"])+".l2l2.dat"
f_CG_l2l2 = open(outfile_name_CG_l2l2, "w")

if options["gcc"]:
    outfile_name_GCC_min = "out/nze2_"+str(options["geometry"])+"_GCC_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_ktls"+str(options["ktls"])+".min.dat"
    f_GCC_min = open(outfile_name_GCC_min, "w")

    outfile_name_GCC_max = "out/nze2_"+str(options["geometry"])+"_GCC_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_ktls"+str(options["ktls"])+".max.dat"
    f_GCC_max = open(outfile_name_GCC_max, "w")

    outfile_name_GCC_l2f = "out/nze2_"+str(options["geometry"])+"_GCC_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_ktls"+str(options["ktls"])+".l2f.dat"
    f_GCC_l2f = open(outfile_name_GCC_l2f, "w")

    outfile_name_GCC_l2l2 = "out/nze2_"+str(options["geometry"])+"_GCC_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_ktls"+str(options["ktls"])+".l2l2.dat"
    f_GCC_l2l2 = open(outfile_name_GCC_l2l2, "w")

#for i_s in range(1,6):
for i_s in [2]:
    for i_t in [0,2,4]:
        print("Starting with tuple ", i_t, i_s)
        l2finalDG, l2l2erDG, minnzesDG, maxnzesDG = SolveDG(i_t=i_t, i_s=i_s, k_t = options["kt"], k_t_lset = options["ktls"], k_s = options["ks"], geom=options["geometry"], gamma=0.05, struct_mesh=options["struct_mesh"], exact_nze_est = True)
        print("Single run result (DG): ", l2finalDG, l2l2erDG, minnzesDG, maxnzesDG)
        f_DG_min.write(str(minnzesDG)+"\t")
        f_DG_max.write(str(maxnzesDG)+"\t")
        f_DG_l2f.write(str(l2finalDG)+"\t")
        f_DG_l2l2.write(str(l2l2erDG)+"\t")
        
        l2finalCG, l2l2erCG, minnzesCG, maxnzesCG = SolveCG(i_t=i_t, i_s=i_s, k_t = options["kt"], k_t_lset = options["ktls"], k_s = options["ks"], geom=options["geometry"], gamma=0.05, struct_mesh=options["struct_mesh"], eps_f=1.1, tp_gp = options["tp_gp"], exact_nze_est = True)
        print("Single run result (CG): ", l2finalCG, l2l2erCG, minnzesCG, maxnzesCG)
        f_CG_min.write(str(minnzesCG)+"\t")
        f_CG_max.write(str(maxnzesCG)+"\t")
        f_CG_l2f.write(str(l2finalCG)+"\t")
        f_CG_l2l2.write(str(l2l2erCG)+"\t")
        
        if options["gcc"]:
            l2finalGCC, l2l2erGCC, minnzesGCC, maxnzesGCC = SolveGCC(i_t=i_t, i_s=i_s, k_t = options["kt"], k_t_lset = options["ktls"], k_s = options["ks"], geom=options["geometry"], gamma=0.05, struct_mesh=options["struct_mesh"], eps_f=1.1, exact_nze_est = True)
            print("Single run result (GCC): ", l2finalGCC, l2l2erGCC, minnzesGCC, maxnzesGCC)
            f_GCC_min.write(str(minnzesGCC)+"\t")
            f_GCC_max.write(str(maxnzesGCC)+"\t")
            f_GCC_l2f.write(str(l2finalGCC)+"\t")
            f_GCC_l2l2.write(str(l2l2erGCC)+"\t")
    f_DG_min.write("\n")
    f_DG_max.write("\n")
    f_DG_l2f.write("\n")
    f_DG_l2l2.write("\n")
    f_CG_min.write("\n")
    f_CG_max.write("\n")
    f_CG_l2f.write("\n")
    f_CG_l2l2.write("\n")
    if options["gcc"]:
        f_GCC_min.write("\n")
        f_GCC_max.write("\n")
        f_GCC_l2f.write("\n")
        f_GCC_l2l2.write("\n")
