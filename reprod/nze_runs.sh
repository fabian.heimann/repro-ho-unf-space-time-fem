#!/bin/bash
#SBATCH -p medium
#SBATCH -t 2:00:00
#SBATCH -N 1
#SBATCH -n 8
#SBATCH -C cascadelake

source ~/.bashrc

python3 nze_study.py --struct_mesh 2 --it 2 --is 2 --geometry moving_circle --ks 1 --kt 1 --ktls 1 --tp_gp 0
python3 nze_study.py --struct_mesh 2 --it 2 --is 2 --geometry moving_circle --ks 2 --kt 2 --ktls 2 --tp_gp 0
python3 nze_study.py --struct_mesh 2 --it 2 --is 2 --geometry moving_circle --ks 3 --kt 3 --ktls 3 --tp_gp 0

python3 nze_study.py --struct_mesh 2 --it 2 --is 2 --geometry moving_circle --ks 1 --kt 1 --ktls 1 --tp_gp 1
python3 nze_study.py --struct_mesh 2 --it 2 --is 2 --geometry moving_circle --ks 2 --kt 2 --ktls 2 --tp_gp 1
python3 nze_study.py --struct_mesh 2 --it 2 --is 2 --geometry moving_circle --ks 3 --kt 3 --ktls 3 --tp_gp 1

python3 nze_study2.py --struct_mesh 2 --geometry moving_circle --ks 1 --kt 1 --ktls 1
python3 nze_study2.py --struct_mesh 2 --geometry moving_circle --ks 3 --kt 3 --ktls 3 -gcc True
python3 nze_study2.py --struct_mesh 2 --geometry moving_circle --ks 5 --kt 5 --ktls 5

python3 process_nze2_table.py
