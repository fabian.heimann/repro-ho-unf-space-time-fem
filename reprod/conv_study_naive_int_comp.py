from xfem import ngsxfemglobals

import argparse

parser = argparse.ArgumentParser(description='Solve convection-diffusion problem on a moving domain by a spacetime method.')
parser.add_argument('-g','--geometry', default="kite", help ='Used geometry, options moving_circle, kite, moving_sphere')
parser.add_argument('-m','--method', default="GCC", help='Used discretisation in time method, options: DG, CG, GCC, default: GCC')
parser.add_argument('-ks','--ks', type=int, default=3, help='order of space discretisation & isoparam mapping, default: 3')
parser.add_argument('-kt','--kt', type=int, default=3, help='order of time discretisation, default: 3. For GCC, 3 will be used independent of this parameter.')
parser.add_argument('-ktls','--ktls', type=int, default=3, help='order of time discretisation for lset deformation, default: 3.')
parser.add_argument('-rs','--refinement_strategy', default="both", help='flag to controll the refinement strategy, options: both, space, time. default: both')
parser.add_argument('-nr','--n_ref', type=int, default=3, help='number of refinements, default: 3')
parser.add_argument('-ro','--ref_offset', type=int, default=0, help='offset of the used level for the non-refined discretisation (space/ time). Only relevant for rs not both. default: 0')
parser.add_argument('-nthr','--nthr', type=int, default=6, help='Number of threads for parallel assembly. default: 6')
parser.add_argument('-sol','--sol', type=str, default="umfpack", help='Direct solver to be used. default: umfpack')
parser.add_argument('-gamma','--gamma', type=float, default=0.05, help='Ghost Penalty stabilisation constant, default: 0.05')
parser.add_argument('-sm','--struct_mesh', type=int, default=1, help='Use a structured mesh, enter either int 0 for false or int 1 for true. default: 1 - true')
parser.add_argument('-cmd','--cmd', type=int, default=0, help='Calc Max Distance. Activate with 1, deactivate with 0. Only available with DG. default: 0')
parser.add_argument('-nti', '--nti', type=int, default=0, help='Use Naive time-integration for benchmarking purposes')

args = parser.parse_args()
options = vars(args)

from conv_study import conv_study

ngsxfemglobals.non_conv_warn_msg_lvl = 1

ngsxfemglobals.do_naive_timeint = False
conv_study(options, special_name = "tint_proper")

ngsxfemglobals.do_naive_timeint = True
ngsxfemglobals.naive_timeint_order = 2*options["kt"] + 2
ngsxfemglobals.naive_timeint_subdivs = 1
conv_study(options, special_name = "tint_naive")

ngsxfemglobals.do_naive_timeint = True
ngsxfemglobals.naive_timeint_order = 4*options["kt"] + 4
ngsxfemglobals.naive_timeint_subdivs = 1
conv_study(options, special_name = "tint_naivedo")

ngsxfemglobals.do_naive_timeint = True
ngsxfemglobals.naive_timeint_order = 2*options["kt"] + 2
ngsxfemglobals.naive_timeint_subdivs = 10
conv_study(options, special_name = "tint_naiveS10")
