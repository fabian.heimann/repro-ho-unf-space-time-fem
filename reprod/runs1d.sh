#!/bin/bash
#SBATCH -p medium
#SBATCH -t 4:00:00
#SBATCH -N 1
#SBATCH -n 8
#SBATCH -C cascadelake

source ~/.bashrc

python3 conv_study.py --geometry moving_int --method DG --ks 1 --kt 1 --ktls 1 --n_ref 12 --struct_mesh 1
python3 conv_study.py --geometry moving_int --method DG --ks 2 --kt 2 --ktls 2 --n_ref 11 --struct_mesh 1
python3 conv_study.py --geometry moving_int --method DG --ks 3 --kt 3 --ktls 3 --n_ref 10 --struct_mesh 1
python3 conv_study.py --geometry moving_int --method DG --ks 4 --kt 4 --ktls 4 --n_ref 9 --struct_mesh 1
python3 conv_study.py --geometry moving_int --method DG --ks 5 --kt 5 --ktls 5 --n_ref 8 --struct_mesh 1
python3 conv_study.py --geometry moving_int --method DG --ks 6 --kt 6 --ktls 6 --n_ref 7 --struct_mesh 1
python3 conv_study.py --geometry moving_int --method DG --ks 7 --kt 7 --ktls 7 --n_ref 6 --struct_mesh 1
python3 conv_study.py --geometry moving_int --method DG --ks 8 --kt 8 --ktls 8 --n_ref 5 --struct_mesh 1

python3 conv_study.py --geometry moving_int --method CG --ks 1 --kt 1 --ktls 1 --n_ref 12 --struct_mesh 1
python3 conv_study.py --geometry moving_int --method CG --ks 2 --kt 2 --ktls 2 --n_ref 11 --struct_mesh 1
python3 conv_study.py --geometry moving_int --method CG --ks 3 --kt 3 --ktls 3 --n_ref 10 --struct_mesh 1
python3 conv_study.py --geometry moving_int --method CG --ks 4 --kt 4 --ktls 4 --n_ref 9 --struct_mesh 1
python3 conv_study.py --geometry moving_int --method CG --ks 5 --kt 5 --ktls 5 --n_ref 8 --struct_mesh 1
python3 conv_study.py --geometry moving_int --method CG --ks 6 --kt 6 --ktls 6 --n_ref 7 --struct_mesh 1
python3 conv_study.py --geometry moving_int --method CG --ks 7 --kt 7 --ktls 7 --n_ref 6 --struct_mesh 1
python3 conv_study.py --geometry moving_int --method CG --ks 8 --kt 8 --ktls 8 --n_ref 5 --struct_mesh 1
