#!/bin/bash
#SBATCH -p medium
#SBATCH -t 0:10:00
#SBATCH -N 1
#SBATCH -n 8
#SBATCH -C cascadelake

source ~/.bashrc

python3 conv_study_naive_int_comp.py --geometry moving_int_sp --method DG --ks 4 --kt 4 --ktls 4 --n_ref 7 -rs space -ro -7 -sm 1 -gamma 5e-2
python3 conv_study_naive_int_comp.py --geometry moving_int_sp --method DG --ks 4 --kt 4 --ktls 4 --n_ref 7 -rs space -ro -7 -sm 1 -gamma 5e-10
python3 conv_study_naive_int_comp.py --geometry moving_int_sp --method DG --ks 4 --kt 4 --ktls 4 --n_ref 7 -rs space -ro -7 -sm 1 -gamma 5e-20
