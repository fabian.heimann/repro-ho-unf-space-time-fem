#!/bin/bash
#SBATCH -p medium
#SBATCH -t 24:00:00
#SBATCH -N 1
#SBATCH -n 8
#SBATCH -C cascadelake

source ~/.bashrc

python3 conv_study.py --method DG --ks 3 --kt 1 --ktls 3 --n_ref 6 -rs time -ro -1 --struct_mesh 2
python3 conv_study.py --method CG --ks 3 --kt 1 --ktls 3 --n_ref 6 -rs time -ro -1 --struct_mesh 2

python3 conv_study.py --method DG --ks 5 --kt 2 --ktls 5 --n_ref 5 -rs time -ro -1 --struct_mesh 2
python3 conv_study.py --method CG --ks 5 --kt 2 --ktls 5 --n_ref 5 -rs time -ro -1 --struct_mesh 2

python3 conv_study.py --method DG --ks 2 --kt 1 --ktls 2 --n_ref 7 --struct_mesh 2
python3 conv_study.py --method DG --ks 3 --kt 2 --ktls 3 --n_ref 6 --struct_mesh 2
python3 conv_study.py --method CG --ks 3 --kt 2 --ktls 3 --n_ref 6 --struct_mesh 2
