from xfem import *

from solve_DG import *
from solve_CG import *
from solve_GCC import *

def Solve(method, i_t, i_s, k_t, k_t_ls, k_s, geom, gamma, struct_mesh, n_threads, solver, calc_max_dist=False):
    if method == "DG":
        return SolveDG(i_t, i_s, k_t, k_t_ls, k_s, geom, gamma=gamma, struct_mesh=struct_mesh, n_threads=n_threads, solver=solver, calc_max_dist=calc_max_dist)
    elif method == "CG":
        return SolveCG(i_t, i_s, k_t, k_t_ls, k_s, geom, gamma=gamma, struct_mesh=struct_mesh, n_threads=n_threads, solver=solver)
    elif method == "GCC":
        return SolveGCC(i_t, i_s, k_t, k_t_ls, k_s, geom, gamma=gamma, struct_mesh=struct_mesh, n_threads=n_threads, solver=solver)
    else:
        print("Non-valid method flag ", method)

import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Solve convection-diffusion problem on a moving domain by a spacetime method.')
    parser.add_argument('-g','--geometry', default="kite", help ='Used geometry, options moving_circle, kite, moving_sphere')
    parser.add_argument('-m','--method', default="DG", help='Used discretisation in time method, options: DG, CG, GCC, default: DG')
    parser.add_argument('-ks','--ks', type=int, default=3, help='order of space discretisation & isoparam mapping, default: 3')
    parser.add_argument('-kt','--kt', type=int, default=3, help='order of time discretisation, default: 3. For GCC, 3 will be used independent of this parameter.')
    parser.add_argument('-ktls','--ktls', type=int, default=3, help='order of time discretisation for lset deformation, default: 3.')
    parser.add_argument('-rs','--refinement_strategy', default="both", help='flag to controll the refinement strategy, options: both, space, time. default: both')
    parser.add_argument('-nr','--n_ref', type=int, default=3, help='number of refinements, default: 3')
    parser.add_argument('-gamma','--gamma', type=float, default=0.05, help='Ghost Penalty stabilisation constant, default: 0.05')
    parser.add_argument('-sm','--struct_mesh', type=int, default=0, help='Use a structured mesh, enter either int 0 for false or int 1 for true. 2 for reproduction unstruct meshes. default: 0')
    parser.add_argument('-ro','--ref_offset', type=int, default=0, help='offset of the used level for the non-refined discretisation (space/ time). Only relevant for rs not both. default: 0')
    parser.add_argument('-nthr','--nthr', type=int, default=6, help='Number of threads for parallel assembly. default: 6')
    parser.add_argument('-sol','--sol', type=str, default="umfpack", help='Direct solver to be used. default: umfpack')
    parser.add_argument('-cmd','--cmd', type=int, default=0, help='Calc Max Distance. Activate with 1, deactivate with 0. Only available with DG. default: 0')
    parser.add_argument('-nti', '--nti', type=int, default=0, help='Use Naive time-integration for benchmarking purposes')

    args = parser.parse_args()
    options = vars(args)

def conv_study(options, special_name = ""):
    if special_name != "":
        special_name = "_"+special_name
    
    if options["ktls"] != options["kt"]:
        special_name = special_name + "_ktls" + str(options["ktls"])
    
    if options["refinement_strategy"] != "both":
        special_name = special_name + "_ro" + str(options["ref_offset"])
    
    outfile_name = "out/conv_"+str(options["geometry"])+"_"+str(options["method"])+"_ks"+str(options["ks"])+"_kt"+str(options["kt"])+"_"+str(options["refinement_strategy"])+"_nref"+str(options["n_ref"])+"_gamma"+str(options["gamma"])+"_sm"+str(options["struct_mesh"])+special_name+".dat"
    
    f = open(outfile_name, "w")
    f.close()

    if options["refinement_strategy"] == "both":
        ref_space = True
        ref_time = True
        timei = 0
        spacei = 0
    elif options["refinement_strategy"] == "space":
        ref_space = True
        ref_time = False
        timei = options["n_ref"]+options["ref_offset"]
        spacei = 0
    elif options["refinement_strategy"] == "time":
        ref_space = False
        ref_time = True
        timei = 0
        spacei = options["n_ref"]+options["ref_offset"]
    if bool(options["nti"]):
        ngsxfemglobals.do_naive_timeint = True
        ngsxfemglobals.naive_timeint_order = -2
        ngsxfemglobals.naive_timeint_subdivs = 1

    l2errors = []
    for i in range(options["n_ref"]):
        if bool(options["cmd"]):
            l2final, l2l2er, minnzes, maxnzes, md = Solve(method = options["method"], i_t=timei, i_s=spacei, k_t = options["kt"], k_t_ls = options["ktls"],  k_s = options["ks"], geom=options["geometry"], gamma=options["gamma"], struct_mesh = options["struct_mesh"], n_threads=options["nthr"], solver=options["sol"], calc_max_dist=True)
            f = open(outfile_name, "a")
            f.write(str(i)+"\t"+str(l2final)+"\t"+str(l2l2er)+"\t"+str(minnzes)+"\t"+str(maxnzes)+"\t"+str(md)+"\n")
            f.close()
        else:
            l2final, l2l2er, minnzes, maxnzes = Solve(method = options["method"], i_t=timei, i_s=spacei, k_t = options["kt"], k_t_ls = options["ktls"],  k_s = options["ks"], geom=options["geometry"], gamma=options["gamma"], struct_mesh = options["struct_mesh"], n_threads=options["nthr"], solver=options["sol"], calc_max_dist=False)
            f = open(outfile_name, "a")
            f.write(str(i)+"\t"+str(l2final)+"\t"+str(l2l2er)+"\t"+str(minnzes)+"\t"+str(maxnzes)+"\n")
            f.close()
        print("Single run result: ", l2final, l2l2er, minnzes, maxnzes)

        if ref_space:
            spacei += 1
        if ref_time:
            timei += 1
        l2errors.append( (l2final, l2l2er) )

    print("final l2 error stat: ", l2errors)
    eocs = [log(l2errors[j-1][0]/l2errors[j][0])/log(2) for j in range(1,len(l2errors))]
    print ("eocs (l2final) : ", eocs)
    eocs = [log(l2errors[j-1][1]/l2errors[j][1])/log(2) for j in range(1,len(l2errors))]
    print ("eocs (l2l2) : ", eocs)

if __name__ == "__main__":
    conv_study(options)
