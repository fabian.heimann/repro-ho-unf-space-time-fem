#!/bin/bash
#SBATCH -p medium
#SBATCH -t 48:00:00
#SBATCH -N 1
#SBATCH -n 8
#SBATCH --mem 100GB

source ~/.bashrc

/usr/bin/time -v python3 conv_study.py --method DG --ks 1 --kt 1 --ktls 1 --n_ref 8 -cmd 1 --struct_mesh 2
/usr/bin/time -v python3 conv_study.py --method CG --ks 1 --kt 1 --ktls 1 --n_ref 8 --struct_mesh 2

/usr/bin/time -v python3 conv_study.py --method DG --ks 2 --kt 2 --ktls 2 --n_ref 8 -cmd 1 --struct_mesh 2
/usr/bin/time -v python3 conv_study.py --method CG --ks 2 --kt 2 --ktls 2 --n_ref 8 --struct_mesh 2

/usr/bin/time -v python3 conv_study.py --method DG --ks 3 --kt 3 --ktls 3 --n_ref 8 -cmd 1 --struct_mesh 2
/usr/bin/time -v python3 conv_study.py --method CG --ks 3 --kt 3 --ktls 3 --n_ref 8 --struct_mesh 2
/usr/bin/time -v python3 conv_study.py --method GCC --ks 3 --kt 3 --ktls 3 --n_ref 8 --struct_mesh 2

/usr/bin/time -v python3 conv_study.py --method DG --ks 4 --kt 4 --ktls 4 --n_ref 7 -cmd 1 --struct_mesh 2
/usr/bin/time -v python3 conv_study.py --method CG --ks 4 --kt 4 --ktls 4 --n_ref 7 --struct_mesh 2

/usr/bin/time -v python3 conv_study.py --method DG --ks 5 --kt 5 --ktls 5 --n_ref 6 -cmd 1 --struct_mesh 2
/usr/bin/time -v python3 conv_study.py --method CG --ks 5 --kt 5 --ktls 5 --n_ref 6 --struct_mesh 2

/usr/bin/time -v python3 conv_study.py --method DG --ks 6 --kt 6 --ktls 6 --n_ref 5 -cmd 1 --struct_mesh 2
/usr/bin/time -v python3 conv_study.py --method CG --ks 6 --kt 6 --ktls 6 --n_ref 5 --struct_mesh 2
