#!/bin/bash
#SBATCH -p fat
#SBATCH -t 48:00:00
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem 200GB

source ~/.bashrc

/usr/bin/time -v python3 conv_study.py --geometry moving_sphere --method DG --ks 1 --kt 1 --ktls 1 --n_ref 6 --sol pardiso --struct_mesh 2
/usr/bin/time -v python3 conv_study.py --geometry moving_sphere --method DG --ks 2 --kt 2 --ktls 2 --n_ref 5 --sol pardiso --struct_mesh 2
/usr/bin/time -v python3 conv_study.py --geometry moving_sphere --method DG --ks 3 --kt 3 --ktls 3 --n_ref 4 --sol pardiso --struct_mesh 2
/usr/bin/time -v python3 conv_study.py --geometry moving_sphere --method DG --ks 4 --kt 4 --ktls 4 --n_ref 3 --sol pardiso --struct_mesh 2

/usr/bin/time -v python3 conv_study.py --geometry moving_sphere --method CG --ks 1 --kt 1 --ktls 1 --n_ref 6 --sol pardiso --struct_mesh 2
/usr/bin/time -v python3 conv_study.py --geometry moving_sphere --method CG --ks 2 --kt 2 --ktls 2 --n_ref 5 --sol pardiso --struct_mesh 2
/usr/bin/time -v python3 conv_study.py --geometry moving_sphere --method CG --ks 3 --kt 3 --ktls 3 --n_ref 4 --sol pardiso --struct_mesh 2
/usr/bin/time -v python3 conv_study.py --geometry moving_sphere --method CG --ks 4 --kt 4 --ktls 4 --n_ref 3 --sol pardiso --struct_mesh 2

/usr/bin/time -v python3 conv_study.py --geometry moving_sphere --method GCC --ks 3 --kt 3 --ktls 3 --n_ref 4 --sol pardiso --struct_mesh 2
