# Reproduction material for "GEOMETRICALLY HIGHER ORDER UNFITTED SPACE-TIME METHODS FOR PDES ON MOVING DOMAINS" by F. Heimann, C. Lehrenfeld and J. Preuß

This repository contains the scripts necessary to reproduce the data of the numerical experiments of the paper. Also, interactive Jupyter notebooks are provided which allow for a more accessible introduction to our numerical software.

The computations are based on the software ngsxfem, where we used the commit 9331ef0c521724980918b27a34a7850bec7e4598.

## Setup ngsxfem to run the examples.
For reproducibility we recommend to use the Docker image of ngsxfem also used in `binder/Dockerfile`, but you may also follow the installation instructions of [`ngsxfem`](http://github.com/ngsxfem/ngsxfem) for a local installation.

## Jupyter Notebooks
Two Jupyter notebooks are provided:

* `st_conv_interactive.ipynb` : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Ffabian.heimann%2Frepro-ho-unf-space-time-fem.git/HEAD?filepath=st_conv_interactive.ipynb)
[![Voila](https://img.shields.io/badge/launch-voila-blueviolet)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Ffabian.heimann%2Frepro-ho-unf-space-time-fem.git/HEAD?urlpath=voila%2Frender%2Fst_conv_interactive.ipynb)
This notebook allows the user to perform simple convergence studies, whilst varying the most important numerical discretisation parameters. For performance reasons, 1+1D experiments are standard, but the cautious/ patient user can also switch to 2+1D examples.

* `solve_DG_nb.ipynb` : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Ffabian.heimann%2Frepro-ho-unf-space-time-fem.git/HEAD?filepath=solve_DG_nb.ipynb)
In this notebook, the user will find the code for a single time-loop run of the DG method, involving explanations of all substeps/ ngsxfem routines involved.

## Reproduction scripts
The script infrastructure to reproduce the numerical data of the paper is given in the folder `reprod`. In order to avoid numerical differences due to different (but similarly suitable) meshes, the scripts exploit pre-generated and loaded meshes. Because of file size reasons, the two largest 3D meshes are not contained and can be downloaded from https://doi.org/10.25625/LSA3U0 .

To reproduce the data of the paper, the bash scripts (`*.sh`) in the `reprod` folder need to be executed. At the same time, these can be used to start the more involved computations on a cluster running with slurm. Note that memory and time consumptions might depend on the specific hardware and that we typically split the sequence of python calls into blocks, where each of these alone might need the specified runtime.

In general, most of these bash scripts will call python scripts also in the folger `reprod`, which are most often just a parameter variation interface for the `Solve*` functions defined in the python files `solve_DG.py` `solve_CG.py`, `solve_GCC.py`. As the similarity in names suggests, the file `solve_DG.py` roughly amounts to the Juyper Notebook `solve_DG_nb.ipynb` containing more explanations.
